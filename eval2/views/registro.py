from django.shortcuts import render

def registro_index(request):
    print('registro_index')
    return render(request, 'registro.html')

def formularioregistro_contacto(request):
    print('formularioregistro_contacto')

    if request.method == 'GET':
        print('invocación por método GET')
        run = request.GET.get('run')
        print('run {0}'.format(run))

    elif request.method == 'POST':
        print('invocación por método POST')
        run = request.POST.get('run')
        print('run {0}'.format(run))

    return render(request, 'registro.html')