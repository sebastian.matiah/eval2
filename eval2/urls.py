"""eval2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from eval2.views.home import index
from eval2.views.galeria import galeria_index
from eval2.views.contacto import contacto_index, formulario_contacto
from eval2.views.registro import registro_index, formularioregistro_contacto


urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', index),
    path('', index),
    path('galery/', galeria_index),
    path('contact/', contacto_index),
    path('contacto/formulario', formulario_contacto),
    path('user/', registro_index),
    path('contacto/registro', formularioregistro_contacto)
]
