function enviar2(){

    let  email = document.getElementById("txt-email").value;
    let  contraseña = document.getElementById("txt-pass1").value;
    let  repetirContraseña = document.getElementById("txt-pass2").value;
    let  nombres = document.getElementById("txt-nombres").value;
    let  apellidos = document.getElementById("txt-apellidos").value;
    let  rut = document.getElementById("txt-rut").value;
    let  dv = document.getElementById("txt-dv").value;
    let  dia_registro = document.getElementById("txt-dia_registro").value;
    let  mes_registro = document.getElementById("txt-mes_registro").value;
    let  anio_registro = document.getElementById("txt-anio_registro").value;
    let  provincia_registro = document.getElementById("txt-provinciaregistro").value;
    let  comuna = document.getElementById("txt-comuna").value;
    let  ciudad = document.getElementById("txt-ciudad").value;
    
    if(isEmpty(email) && isEmpty(contraseña) && isEmpty(repetirContraseña) &&
    isEmpty(nombres) && isEmpty(apellidos) && isEmpty(rut) && 
    isEmpty(dv) && isEmpty(dia_registro) && isEmpty(mes_registro) &&
    isEmpty(anio_registro) && isEmpty(provincia_registro) && isEmpty(comuna) &&
    isEmpty(ciudad)
    ){
        console.log('formulario completamente lleno');
        let etiqueta =  document.getElementById("txt-message");
        console.log(etiqueta);
        etiqueta.innerHTML = '<div style="margin-top:25px;" class="alert alert-success alert-dismissible fade show" role="alert">'+
                'Formulario enviado con éxito' +
                //'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                //'<span aria-hidden="true">\&times;</span></button>'
                '</div>';
        document.getElementById('frm-contacto').submit();
    }else{
        console.log('formulario con campos vacios');
        let etiqueta =  document.getElementById("txt-message");
        etiqueta.innerHTML = '<div style="margin-top:25px;" class="alert alert-danger alert-dismissible fade show" role="alert">'+
                'Error debe ingresar todos los campos' +
                //'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                //'<span aria-hidden="true">\&times;</span></button>'
                '</div>';       
    }
}

function isEmpty(element){
    if(element !== undefined && element !== '' ){
        return true;
    }else{
        return false;
    }
}